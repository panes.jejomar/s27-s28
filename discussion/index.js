const protocol = require('http');



//identify and designate a 'virtual point' where the network connection will start and end.
const port = 4000;
//bind using listen function

// create a connection
protocol.createServer((request,response) => {
    //describe the response when the client interacts and communicates with the server
    response.write(`Welcome to the server`);
    response.end();
}).listen(port)

console.log(`server is running on port ${port}`);

